/*!
 * Vallenato 1.0
 * A Simple JQuery Accordion
 *
 * Designed by Switchroyale
 * 
 * Use Vallenato for whatever you want, enjoy!
 */

jQuery(document).ready(function()
{
	//Add Inactive Class To All Accordion Headers
	jQuery('.accordion-header').toggleClass('inactive-header');

	//Set The Accordion Content Width
	var contentwidth = jQuery('.accordion-header').width();
	//jQuery('.accordion-content').css({'width' : contentwidth });

	//Open The First Accordion Section When Page Loads
	jQuery('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	jQuery('.accordion-content').first().slideDown().toggleClass('open-content');

	jQuery('.accordion-content').first().parent().addClass('mix-active');

	// The Accordion Effect
	jQuery('.accordion-header').click(function () {
		if(jQuery(this).is('.inactive-header')) {
			jQuery('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			jQuery(this).toggleClass('active-header').toggleClass('inactive-header');
			jQuery(this).next().slideToggle().toggleClass('open-content');
			// Custom by Manh: Active
			jQuery('.mix').removeClass('mix-active');
			jQuery(this).parent().addClass('mix-active');
		}
		else {
			jQuery(this).toggleClass('active-header').toggleClass('inactive-header');
			jQuery(this).next().slideToggle().toggleClass('open-content');
			// Custom by Manh: Remove Active
			jQuery(this).parent().removeClass('mix-active');
		}
	});
	return false;
});