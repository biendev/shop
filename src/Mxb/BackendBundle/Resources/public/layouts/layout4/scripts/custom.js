/**
Demo script to handle the theme demo
**/
var Custom = function () {

    // Handle Theme Settings
    var showRowContent = function () {
      
      /*$('.js-edit').each(function(){
        var _this = $(this);
        //var btnEdit = _this.find('.edit');
        var value = _this.find('.value').text();        
        var input = '<input type="text" class="form-control input-inline" value="' + value + '">';
        
        $(document).on('click', '.edit', function(){
          var value = $(this).closest('.js-edit').find('.value').text();        
          var input = '<input type="text" class="form-control input-inline" value="' + value + '">';
        
          $(this).closest('.js-edit').find('.value').html(input);
          $(this).parent().find('.cancel, .save').css('display', 'inline-block');
          $(this).parent().find('.edit').css('display', 'none');
        });
        
        $(document).on('click', '.cancel', function(){                
          $(this).closest('.js-edit').find('.value').html(value);
          $(this).parent().find('.cancel, .save').css('display', 'none');
          $(this).parent().find('.edit').css('display', 'inline-block');
        });
        
        $(document).on('click', '.save', function(){
          var newValue = $(this).closest('.js-edit').find('.form-control').val();
          $(this).closest('.js-edit').find('.value').html(newValue);
          $(this).parent().find('.cancel, .save').css('display', 'none');
          $(this).parent().find('.edit').css('display', 'inline-block');
        });
      });*/
      
      $(document).on('click', '.view-row-content', function(){
        $(this).closest('tr').next('.row-content').fadeIn();
      });
      
    };
		 // Handle Theme Settings
    var rowExpanded = function () {
      $(document).on('click', '.rowItemBtn', function() {
				var _this = $(this);
				var _parent = _this.closest('.rowItem');
				
				if(_parent.hasClass('open')) {
					_parent.next('.rowItemExpand').slideUp();
					_parent.removeClass('open');
          
          if(_this.hasClass('rowItemBtnText')) {
            _this.find('.text').html('VIEW MORE');
          }
				}else{
					_parent.addClass('open');
					_parent.next('.rowItemExpand').slideDown();
          
          if(_this.hasClass('rowItemBtnText')) {
            _this.find('.text').html('VIEW LESS');
          }
				}
			})
        
    };
		
		var switchChange = function() {
			$('.make-switch').on('switchChange.bootstrapSwitch',function(){
				var _this = $(this);
				var _parent =  _this.closest('.bootstrap-switch');
				var _labelText = _parent.next('.switch-text');
				
				if(_this.prop('checked', true)) {
				}
				
				if(_parent.hasClass('bootstrap-switch-off')) {
					_labelText.find('.switch-text-on').hide();
					_labelText.find('.switch-text-off').show();
				}else {
					_labelText.find('.switch-text-off').hide();
					_labelText.find('.switch-text-on').show();
				}
			})
		};

    return {

        //main function to initiate the theme
        init: function() {
            // handles style customer tool
            showRowContent(); 
						rowExpanded(); 
						switchChange();
        }
    };

}();

function showOverLay(){
  $('.blink-loader').css({'opacity':0.7, 'visibility': 'visible'});
  
  $(document).on('click', function(){
    $('.blink-loader').css({'opacity':0, 'visibility': 'hidden'});
  });
  
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {    
       Custom.init(); // init metronic core componets
    });
}