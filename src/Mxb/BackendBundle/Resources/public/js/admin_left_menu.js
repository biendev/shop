
var Menu = {
    curentRoute: $("#current-route-data").val(),
    data: {},

    initStatus: function () {
        this.activeRoute(this.curentRoute);
        if ($('.page-sidebar-menu').find('li  .active').size() == 0) {
            var route = this.getActiveRouteDefault();
            this.activeRoute(route);
        }

    },
    activeRoute: function (route) {

        $.each($('.page-sidebar-menu').find('li a'), function (index, element) {
            if (route == $(element).attr('href')) {

                var curLi = $(element).parent();
                curLi.find('.arrow').first().addClass('open');
                if (curLi.parent().hasClass('sub-menu')) {
                    curLi.addClass('active open');
                    var subMenu = curLi.parent().parent();
                    subMenu.addClass('active open');
                    subMenu.find('.arrow').first().addClass('open');
                } else {
                    curLi.addClass('active open');
                }
            }
        });
    },
    getActiveRouteDefault: function () {
        if(typeof this.curentRoute != "undefined"){
            var routeArray = this.curentRoute.split('/');
            var length = routeArray.length;
            var activeRoute = [];
            var check = false;
            for (var i = 0; i < length; i++) {
                activeRoute.push(routeArray[i]);
                if (check) {
                    break;
                }
                if (routeArray[i] == 'admin') {
                    check = true;
                }
            }
            return activeRoute.join('/');
        }
        return '';
    }
};

$(document).ready(function () {

    Menu.initStatus();
});
