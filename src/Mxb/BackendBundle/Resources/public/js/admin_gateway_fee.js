var Form = {
    baseUrl: $("#asset-url").val(),
    data: {},

    initStatus: function () {
        $(".btn-sm").on('click', function (e) {
            e.preventDefault();
            $(this).parents('form').first().submit();
        });
        this.validateInfor('form-1');
        this.validateInfor('form-2');
        this.validateInfor('form-3');
        this.validateInfor('form-4');
        this.validateInfor('form-5');
        this.validateInfor('form-6');
        this.validateInfor('form-7');
        
    },    

    validateInfor: function (id) {
        $("#"+id).validate({
            errorClass: "error",
            errorElement: 'span',
            errorPlacement: function (error, element) {
                var $e = element;
                switch (element.attr("name")) {
                    case 'admin_fee[fee]':
                        var tag = $e.parents(".input-medium-wrap").first();                   
                        tag.append(error);
                        break;
                    case 'admin_fee[date]':
                        var tag = $e.parents(".date-picker").first();                       
                        error.insertAfter(tag);
                        break;
                    

                    default :
                        error.insertAfter(element);
                        break;
                }

            },
            rules: {
                'admin_fee[fee1]': {
                    number: true
                }
               
            },
            submitHandler: function (form) {
                Form.submitData(form, 3);
            }
        });
        
      
    },
    submitData: function(form,type) {
        var dependUrl = $("#ajax-url").val();
        var form_data = new FormData();
        form_data.append('type', type);
        $.each($(form).serializeArray(), function (index, value) {
            form_data.append(value.name, value.value);
        });
      
        $.ajax({
            type: "POST",
            url: dependUrl,
            data: form_data,
            contentType: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (data, textStatus, jqXHR) {
                if(data.success) {
                    var t = '<span id="" class="">Current value is'+data.value+'</span>';

                    $(t).insertAfter($(form).find('.input-group').first());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (typeof errorCallback == 'function')
                    return errorCallback(jqXHR, textStatus, errorThrown);
                return false;
            }
        });
    }
};


$(document).ready(function () {
  

    Form.initStatus();
});
