var Form = {
    baseUrl: $("#asset-url").val(),
    data: {},

    initStatus: function () {
      
        this.validateInfor();
        $("#admin_pharmacy_country").on('change', function () {
            var dependUrl = $("#ajaxUrlDependent").val();
            var dataPost = {'type': 1, 'data': this.value};
            Form.callAjaxForSelect2(dependUrl, '#admin_pharmacy_state', dataPost, 'Select State / Province');
            dataPost = {'type': 6, 'data': this.value};
            Form.callAjaxForSelect2(dependUrl, '#admin_pharmacy_city', dataPost, 'Select City');
        });
        $("#admin_pharmacy_state").on('change', function () {
            var dependUrl = $("#ajaxUrlDependent").val();
            var dataPost = {'type': 2, 'data': this.value};
            Form.callAjaxForSelect2(dependUrl, '#admin_pharmacy_city', dataPost, 'Select City');
        });
       
        $("#admin_pharmacy_state").select2({placeholder: 'Select State / Province'});
        $("#admin_pharmacy_country").select2({placeholder: 'Select Country'});
        $("#admin_pharmacy_city").select2({placeholder: 'Select City'});
        $("#admin_pharmacy_bankCountry").select2({placeholder: 'Select Country'});
        
    },

    getListCountryPhone(input) {
        if (this.countryData.length == 0) {
            var dependUrl = $("#ajaxUrlDependent").val();
            var dataPost = {'type': 3};
            $.ajax({
                type: "POST",
                url: dependUrl,
                data: dataPost,
                beforeSend: function () {
                },
                success: function (data, textStatus, jqXHR) {
                    Form.countryData = data;
                    var t = '';
                    $.each(data, function (index, value) {
                        var slect = '';
                        if (Form.defaultPhoneCode == value.id)
                        {
                            slect = 'selected="selected"'
                        }

                        t += '<option ' + slect + '  value="' + value.id + '">' + value.name + ' (+' + value.phoneCode + ')</option>';
                    });
                    $(input).html(t);
                    $(input).select2();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (typeof errorCallback == 'function')
                        return errorCallback(jqXHR, textStatus, errorThrown);
                    return false;
                }
            });
        } else {
            var data = Form.countryData;
            var t = '';
            $.each(data, function (index, value) {
                var slect = '';
                if (Form.defaultPhoneCode == value.phoneCode)
                {
                    slect = 'selected="selected"'
                }
                t += '<option ' + slect + ' data-countrycode="' + value.code + '" value="' + value.phoneCode + '">' + value.name + ' (+' + value.phoneCode + ')</option>';
            });
            $(input).html(t);
            $(input).select2();
        }
    },

    callAjaxForSelect2: function (url, target, dataPost, empty, val) {
        $.ajax({
            type: "POST",
            url: url,
            data: dataPost,
            beforeSend: function () {
            },
            success: function (data, textStatus, jqXHR) {
                var t = '';
                $.each(data, function (index, value) {
                    t += '<option value="' + index + '">' + value + '</option>';
                });
                $(target).html(t);
                $(target).val(val);
                $(target).select2({
                    placeholder: empty,
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (typeof errorCallback == 'function')
                    return errorCallback(jqXHR, textStatus, errorThrown);
                return false;
            }
        });
    },

    validateInfor: function () {
        var validobj = $("#admin-create-pharmacy").validate({
            errorClass: "error",
            errorElement: 'span',
            errorPlacement: function (error, element) {
                var $e = element;
                switch (element.attr("name")) {
                    case 'admin_pharmacy[phoneLocation]':
                        var tag = $e.parents(".row").first().find('.phone-error-notice');
                        tag.html('');
                        tag.append(error);
                        break;
                    case 'admin_pharmacy[phoneArea]':
                        var tag = $e.parents(".row").first().find('.phone-error-notice');
                        tag.html('');
                        tag.append(error);
                        break;
                    case 'admin_pharmacy[phone]':
                        var tag = $e.parents(".row").first().find('.phone-error-notice');
                        tag.html('');
                        tag.append(error);
                        break;
                    
                    case 'admin_pharmacy[country]':

                        $e.parent().append(error);
                        break;
                    case 'admin_pharmacy[bankCountry]':

                        $e.parent().append(error);
                        break;
                    case 'admin_pharmacy[city]':
                        console.log($e);
                        $e.parent().append(error);
                        break;              

                    default :
                        error.insertAfter(element);
                        break;
                }

            },
            rules: {
                'admin_pharmacy[phoneArea]': {
                    digits: true
                },
                'admin_pharmacy[phone]': {
                    digits: true
                },
                
            },
            submitHandler: function (form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {

                var elem = $(element);
                if (elem.hasClass('select2')) {
                    var t = elem.parent().find(".select2-selection").first();
                    t.attr('style', 'border: 1px solid red');

                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass('select2')) {
                    console.log(elem);
                    var t = elem.parent().find(".select2-selection").first();
                    t.attr('style', '');
                } else {
                    elem.removeClass(errorClass);
                }
            }

        });
        $(document).on('change', '.select2', function () {
            if (!$.isEmptyObject(validobj.submitted)) {
                validobj.form();
            }
        });
        $("#admin_pharmacy_phoneArea").on('change', function () {
            if (!$.isEmptyObject(validobj.submitted)) {
                validobj.form();
            }
        });
        $("#admin_pharmacy_phone").on('change', function () {
            if (!$.isEmptyObject(validobj.submitted)) {
                validobj.form();
            }
        });
        $("#admin_pharmacy_localIdPassportDate").on('change', function () {
            if (!$.isEmptyObject(validobj.submitted)) {
                validobj.form();
            }
        });
    }
};

$(document).ready(function () {
    jQuery.validator.addMethod("empty", function (value, element) {
        return value != 'empty';
    }, "This field is required");

   
    Form.initStatus();
});
