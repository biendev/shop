/**
 * Rx Refill Reminder Settings
 * Author Luyen Nguyen
 * Date: 08/28/2017
 */
// Validation message
var mssgError = 'Reminder days is required';
// Init function
var jsReminder = {
    init: function () {
        this.reminderValidate();
    },
    reminderValidate: function () {
        $("#frm-reminder-thirty").validate({
            rules: {
                "rx_refill_reminder_setting[reminderthirtydays]": {
                    required: true,
                    min: 0,
                    max: 30,
                    number: true
                }
            },
            messages: {
                "rx_refill_reminder_setting[reminderthirtydays]": {
                    required: mssgError
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.parents('div.reminder-text').addClass('error');
                error.insertAfter(element.parents('div.reminder-text'));
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorClass: "error"
        });
        $("#frm-reminder-sixty").validate({
            rules: {
                "rx_refill_reminder_setting[remindersixtydays]": {
                    required: true,
                    min: 0,
                    max: 180,
                    number: true
                }
            },
            messages: {
                "rx_refill_reminder_setting[remindersixtydays]": {
                    required: mssgError
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.parents('div.reminder-text').addClass('error');
                error.insertAfter(element.parents('div.reminder-text'));
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorClass: "error"
        });
    }
};

$(document).ready(function () {
    jsReminder.init();
});