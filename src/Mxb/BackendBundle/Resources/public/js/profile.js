var jsProfile = {

    piForm: '#personal-information-form',
    pwForm: '#change-password-form',
    profileData : '',
    profileAjaxUrl : $('#ajax_profile_change').val(),
    passwordData : '',
    passwordAjaxUrl : $('#ajax_password_change').val(),

    init: function() {

        jsProfile.profileAjaxUrl = $('#ajax_profile_change').val();
        jsProfile.submitFormPI();
        jsProfile.validatePassword();
        jsProfile.submitFormPassword();
        jsProfile.validateRequiredOnChange();

    },

    submitFormPassword: function(){
        $('#btn-submit-pwd').on('click',function(e){

            $(jsProfile.pwForm).find('input[type="password"]').each(function(e){
                if($(this).val() == ''){
                    jsProfile.showError($(this), 'Please input this field.');
                }
            });

            if($(jsProfile.pwForm).find('span.error:not(".hidden")').length > 0){
                e.preventDefault();
            }else{
                jsProfile.passwordData = $(jsProfile.pwForm).serializeArray();
                $.ajax({
                    type: "POST",
                    url: jsProfile.passwordAjaxUrl,
                    data: jsProfile.passwordData,
                    success: function(data){
                        $(jsProfile.pwForm)[0].reset();
                        if(data.success){
                            $('#modal-notice-successful').modal('show');
                        }else{
                            $('#modal-notice-unsuccessful').find('.txt-notice').html(data.message);
                            $('#modal-notice-unsuccessful').modal('show');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#modal-notice-unsuccessful').modal('show');
                        if (typeof errorCallback == 'function')
                            return errorCallback(jqXHR, textStatus, errorThrown);
                        return false;
                    },
                    beforeSend: function () {
                    },
                });

            }
        });
    },

    submitFormPI: function(){
        $('#btn-submit-pi').on('click',function(e){
            jsProfile.validateEmail();
            jsProfile.validateRequired();
            jsProfile.profileData = $(jsProfile.piForm).serializeArray();
            
            if($(jsProfile.piForm).find('span.error:not(".hidden")').length > 0){
                e.preventDefault();
            }else{

                var imgname = $('#ProfileBundle_admin_image').val();
                var profile = '';

                var fd = new FormData();

                if(imgname)
                {
                    var ext = imgname.substr((imgname.lastIndexOf('.') + 1));
                    var size = $('#ProfileBundle_admin_image')[0].files[0].size;
                    if (ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'PNG' || ext == 'JPG' || ext == 'JPEG')
                    {
                        if (size <= 1000000)
                        {
                            profile = $('#ProfileBundle_admin_image')[0].files[0];
                        }
                    }
                }

                $.each(jsProfile.profileData,function(key,input){
                    fd.append(input.name,input.value);
                });
                
                fd.append('image', profile ); 

                $.ajax({
                    type: "POST",
                    url: jsProfile.profileAjaxUrl,
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        if(data.success){
                            $('#modal-notice-successful').modal('show');
                        }else{
                            $('#modal-notice-unsuccessful').find('.txt-notice').html(data.message);
                            $('#modal-notice-unsuccessful').modal('show');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#modal-notice-unsuccessful').modal('show');
                        if (typeof errorCallback == 'function')
                            return errorCallback(jqXHR, textStatus, errorThrown);
                        return false;
                    },
                    beforeSend: function () {
                    },
                });

            }
        });
    },

    validateRequiredOnChange: function(){
        $(jsProfile.piForm).find('input:not("#ProfileBundle_admin_email"), select').each(function(e){
            $(this).on('change', function(){
                if($(this).is('input') && typeof $(this).attr("required") !== typeof undefined && $(this).attr("required") !== false  && $(this).val() == '' ){
                    if($(this).data('required') == '' || $(this).data('required') == undefined){
                        jsProfile.showError($(this), 'This field is required.');
                    }
                }else{
                    jsProfile.removeError($(this));
                }
            });
        });

        $('#ProfileBundle_admin_email').on('keydown', function(e){
            jsProfile.validateEmail();
        });
    },

    validateRequired: function(){
        $(jsProfile.piForm).find('input, select').each(function(e){
            if($(this).is('input') && typeof $(this).attr("required") !== typeof undefined && $(this).attr("required") !== false  && $(this).val() == '' ){
                if($(this).data('required') == '' || $(this).data('required') == undefined){
                    jsProfile.showError($(this), 'This field is required.');
                }
            }
        });
    },

    validateEmail: function(){
        var filter = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        var email = $('#ProfileBundle_admin_email').val();
        if ($.trim(email).length == 0) {
            jsProfile.showError($('#ProfileBundle_admin_email'),'Please enter valid email address.');
        } else if (!filter.test(email)) {
            jsProfile.showError($('#ProfileBundle_admin_email'),'Invalid email address.');
        } else {
            jsProfile.removeError($('#ProfileBundle_admin_email'));
        }
    },

    showError: function(el, msg){
        var div = el.closest('div[class*=col-md-]').find('div.error');
        el.addClass('error');
        div.html('<span class="error">'+msg+'</span>');
    },

    removeError: function(el){
        var div = el.closest('div[class*=col-md-]').find('div.error');
        el.removeClass('error');
        div.find('span.error').addClass('hidden');
    },

    validatePassword: function(){

        $('#ChangePasswordBundle_agent_current_password, #ChangePasswordBundle_agent_new_password, #ChangePasswordBundle_agent_confirm_password').on('keyup', function(){
            jsProfile.removeError($(this));
        });

        $('#ChangePasswordBundle_agent_new_password').on('keyup', function(){
            //Minimum eight characters, at least one uppercase letter, one lowercase letter and one number
            var regex = /^(?=.*\d)(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            if(!regex.test($(this).val()) && $(this).val() != ''){   
                jsProfile.showError($(this),'Minimum eight characters, at least one uppercase letter, one lowercase letter and one number.');
            }else{
                jsProfile.removeError($(this));
            }

            if($(this).val() != $('#ChangePasswordBundle_agent_confirm_password').val() && $('#ChangePasswordBundle_agent_confirm_password').val() != ''){
                jsProfile.showError($('#ChangePasswordBundle_agent_confirm_password'),'Password does not match.');
            }
        });

        $('#ChangePasswordBundle_agent_confirm_password').on('keyup', function(){
            if($(this).val() != $('#ChangePasswordBundle_agent_new_password').val() && $(this).val() != ''){   
                jsProfile.showError($(this),'Password does not match.');
            }else{
                jsProfile.removeError($(this));
            }
        });
    }
}

$(document).ready(function() {
    jsProfile.init();
});