var Form = {
    baseUrl: $("#asset-url").val(),
    data: {},

    initStatus: function () {
        $(".btn-sm").on('click', function (e) {
            e.preventDefault();
            $(this).parents('form').first().submit();
        });
        this.validateInfor();

    },

    validateInfor: function () {
        var validobj = $("#admin-fee-1").validate({
            errorClass: "error",
            errorElement: 'span',
            errorPlacement: function (error, element) {
                var $e = element;
                switch (element.attr("name")) {
                    case 'admin_fee[fee1]':
                        var tag = $e.parents(".input-medium-wrap").first();
                        tag.append(error);
                        break;
                    case 'admin_fee[date1]':
                        var tag = $e.parents(".date-picker").first();
                        error.insertAfter(tag);
                        break;


                    default :
                        error.insertAfter(element);
                        break;
                }

            },
            rules: {
                'admin_fee[fee1]': {
                    number: true
                }

            },
            submitHandler: function (form) {
                Form.submitData(form, 1);
            }
        });

        $("#admin_fee_date1").on('change', function () {
            if (!$.isEmptyObject(validobj.submitted)) {
                validobj.form();
            }
        });

        var validobj2 = $("#admin-fee-2").validate({
            errorClass: "error",
            errorElement: 'span',
            errorPlacement: function (error, element) {
                var $e = element;
                switch (element.attr("name")) {
                    case 'admin_fee[fee2]':
                        var tag = $e.parents(".input-medium-wrap").first();
                        tag.append(error);
                        break;
                    case 'admin_fee[date2]':
                        var tag = $e.parents(".date-picker").first();
                        error.insertAfter(tag);
                        break;


                    default :
                        error.insertAfter(element);
                        break;
                }

            },
            rules: {
                'admin_fee[fee1]': {
                    number: true
                }

            },
            submitHandler: function (form) {
                Form.submitData(form, 2);
            }
        });

        $("#admin_fee_date2").on('change', function () {
            if (!$.isEmptyObject(validobj2.submitted)) {
                validobj2.form();
            }
        });
    },
    submitData: function (form, type) {
        console.log($(form).serializeArray());
        var dependUrl = $("#ajax-url").val();
        var form_data = new FormData();
        form_data.append('type', type);
        $.each($(form).serializeArray(), function (index, value) {
            form_data.append(value.name, value.value);
        });

        $.ajax({
            type: "POST",
            url: dependUrl,
            data: form_data,
            contentType: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (data, textStatus, jqXHR) {
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (typeof errorCallback == 'function')
                    return errorCallback(jqXHR, textStatus, errorThrown);
                return false;
            }
        });
    }
};


$(document).ready(function () {

    Form.initStatus();
});
