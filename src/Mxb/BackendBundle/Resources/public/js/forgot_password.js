var jsChangePassword = {

    pwForm: '#change-password-form',

    init: function() {
        jsChangePassword.submitFormPassword();
        jsChangePassword.validatePassword('#ChangePasswordBundle_public_new_password','#ChangePasswordBundle_public_confirm_password');
        jsChangePassword.validatePassword('#ChangePasswordBundle_public_new_password','#ChangePasswordBundle_public_confirm_password');
    },

    submitFormPassword: function(){
        $('#btn-submit-pwd').on('click',function(e){
            $(jsChangePassword.pwForm).find('input[type="password"]').each(function(e){
                if($(this).val() == ''){
                    jsChangePassword.showError($(this), 'Please input this field.');
                }
            });
            if($(jsChangePassword.pwForm).find('span.error:not(".hidden")').length > 0){
                e.prevenDefault();
            }else{
                $(jsChangePassword.pwForm).submit();
            }
        });
    },

    showError: function(el, msg){
        var div = el.next('div.errorBox');
        el.addClass('error');
        div.html('<span class="error">'+msg+'</span>');
    },

    removeError: function(el){
        var div = el.next('div.errorBox');
        el.removeClass('error');
        div.find('span.error').addClass('hidden');
    },

    validatePassword: function(newPassword, confirmPassword){

        $(newPassword, confirmPassword).on('keyup', function(){
            jsChangePassword.removeError($(this));
        });

        $(newPassword).on('keyup', function(){
            //Minimum eight characters, at least one uppercase letter, one lowercase letter and one number
            var regex = /^(?=.*\d)(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            if(!regex.test($(this).val()) && $(this).val() != ''){   
                jsChangePassword.showError($(this),'Minimum eight characters, at least one uppercase letter, one lowercase letter and one number.');
            }else{
                jsChangePassword.removeError($(this));
            }

            if($(this).val() != $(confirmPassword).val() && $(confirmPassword).val() != ''){
                jsChangePassword.showError($(confirmPassword),'Password does not match.');
            }
        });

        $(confirmPassword).on('keyup', function(){
            if($(this).val() != $(newPassword).val() && $(this).val() != ''){   
                jsChangePassword.showError($(this),'Password does not match.');
            }else{
                jsChangePassword.removeError($(this));
            }
        });
    }
}

$(document).ready(function() {
    jsChangePassword.init();
});