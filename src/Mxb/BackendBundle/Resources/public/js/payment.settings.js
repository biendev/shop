var jsPSetting = {
    init: function() {
        jsPSetting.platformSetting();
        jsPSetting.psPercentage();
    },
    platformSetting: function(){
        jsCommon.digitPercent();
        $("#frm-product-margin").validate({
            rules : {
                "platform_setting[local]": {
                    required : true,
                    min: 0,
                    max: 100
                },
                "platform_setting[overseas]": {
                    required : true,
                    min: 0,
                    max: 100
                }
            },
            messages : {
                "platform_setting[local]": {
                    required : msg.msgRequiredField.replace('%s', "Local Patient")
                },
                "platform_setting[overseas]": {
                    required : msg.msgRequiredField.replace('%s', "Overseas Patient")
                }
            },
            errorElement : 'span',
            errorPlacement : function(error, element) {
                error.insertAfter(element.parents('div.input-group'));
            },
            submitHandler : function(form) {
                form.submit();
            },
            errorClass : "error"
        });
        $("#frm-gst-rate").validate({
            rules : {
                "platform_setting[newGstRate]": {
                    required : true,
                    min: 0,
                    max: 100
                },
                "platform_setting[gstRateAffectDate]": {
                    required : true
                }
            },
            messages : {
                "platform_setting[newGstRate]": {
                    required : msg.msgRequiredField.replace('%s', "Gst Rate")
                },
                "platform_setting[gstRateAffectDate]": {
                    required : msg.msgRequiredField.replace('%s', "Date")
                }
            },
            errorElement : 'span',
            errorPlacement : function(error, element) {
                error.insertAfter(element.parents('div.input-group'));
            },
            submitHandler : function(form) {
                form.submit();
            },
            errorClass : "error"
        });
        $("#frm-schedule").validate({
            rules : {
                "platform_setting[doctorStatementDate]": {
                    required : true,
                    min: 1,
                    max: 29,
                    number: true
                },
                "platform_setting[agentStatementDate]": {
                    required : true,
                    min: 1,
                    max: 29,
                    number: true
                }
            },
            messages : {
                "platform_setting[doctorStatementDate]": {
                    required : msg.msgRequiredField.replace('%s', "Doctor Statement")
                },
                "platform_setting[agentStatementDate]": {
                    required : msg.msgRequiredField.replace('%s', "Agent Statement")
                }
            },
            errorElement : 'span',
            errorPlacement : function(error, element) {
                error.insertAfter(element.parents('div.input-group'));
            },
            submitHandler : function(form) {
                form.submit();
            },
            errorClass : "error"
        });
    },
    psPercentage: function(){
        $('input.icheck').on('ifChecked', function(event){
            successCallback = function (res) {
                window.location.href = $("#url_payment_gross_margin_share").val()
            };
            errorCallback = function (xhr, ajaxOptions, thrownError) {};
            jsDataService.callAPI($("#url_payment_gms_update_active").val(), {area_type:$(this).val(),is_active:1}, "POST", successCallback, errorCallback, null, 'json');
        }).on('ifUnchecked', function(event){
            successCallback = function (res) {
                window.location.href = $("#url_payment_gross_margin_share").val()
            };
            errorCallback = function (xhr, ajaxOptions, thrownError) {};
            jsDataService.callAPI($("#url_payment_gms_update_active").val(), {area_type:$(this).val(),is_active:0}, "POST", successCallback, errorCallback, null, 'json');
        });
        jsPSetting.gmsValidation("#frm-medicine1");
        jsPSetting.gmsValidation("#frm-medicine2");
        jsPSetting.gmsValidation("#frm-service1");
        jsPSetting.gmsValidation("#frm-service2");
        jsPSetting.gmsValidation("#frm-custom-caf1");
        jsPSetting.gmsValidation("#frm-custom-caf2");
        jsPSetting.gmsValidation("#frm-live-consult1");
        jsPSetting.gmsValidation("#frm-live-consult2");
    },
    gmsValidation: function(element){
        $(element).validate({
            rules : {
                "ps_percentage[platformPercentage]": {
                    required : true,
                    min: 0,
                    max: 100
                },
                "ps_percentage[agentPercentage]": {
                    required : true,
                    min: 0,
                    max: 100
                },
                "ps_percentage[doctorPercentage]": {
                    required : true,
                    min: 0,
                    max: 100
                },
                "ps_percentage[totalPercentage]": {
                    min: 100,
                    max: 100
                },
                "ps_percentage[takeEffectOn]": {
                    required : true
                }
            },
            messages : {
                "ps_percentage[platformPercentage]": {
                    required : msg.msgRequiredField.replace('%s', "GMEDES Sdn Bhd")
                },
                "ps_percentage[agentPercentage]": {
                    required : msg.msgRequiredField.replace('%s', "Agent")
                },
                "ps_percentage[doctorPercentage]": {
                    required : msg.msgRequiredField.replace('%s', "Doctor")
                },
                "ps_percentage[takeEffectOn]": {
                    required : msg.msgRequiredField.replace('%s', "Date")
                }
            },
            errorElement : 'span',
            errorPlacement : function(error, element) {
                error.insertAfter(element.parents('div.input-group'));
            },
            submitHandler : function(form) {
                form.submit();
            },
            errorClass : "error"
        });
        jsPSetting.calTotal(element);
    },
    calTotal: function(element){
        var platformPercentage = "input[name='ps_percentage[platformPercentage]']";
        var agentPercentage = "input[name='ps_percentage[agentPercentage]']";
        var doctorPercentage = "input[name='ps_percentage[doctorPercentage]']";
        var totalPercentage = "input[name='ps_percentage[totalPercentage]']";
        $(element)
        .on('blur', platformPercentage, function(){
            var total = 0;
            var self_form = $(this).closest('form');
            total += parseFloat($(this).val());
            total += parseFloat(self_form.find(agentPercentage).val());
            total += parseFloat(self_form.find(doctorPercentage).val());
            self_form.find(totalPercentage).val(jsCommon.currencyFormat(total));
        })
        .on('blur', agentPercentage, function(){
            var total = 0;
            var self_form = $(this).closest('form');
            total += parseFloat($(this).val());
            total += parseFloat(self_form.find(agentPercentage).val());
            total += parseFloat(self_form.find(doctorPercentage).val());
            self_form.find(totalPercentage).val(jsCommon.currencyFormat(total));
        })
        .on('blur', doctorPercentage, function(){
            var total = 0;
            var self_form = $(this).closest('form');
            total += parseFloat($(this).val());
            total += parseFloat(self_form.find(agentPercentage).val());
            total += parseFloat(self_form.find(platformPercentage).val());
            self_form.find(totalPercentage).val(jsCommon.currencyFormat(total));
        });
    }
};
$(document).ready(function() {
    jsPSetting.init();
});