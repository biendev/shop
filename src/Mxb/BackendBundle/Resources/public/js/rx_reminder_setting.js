var RxReminderSetting = {
    init: function () {
        RxReminderSetting.vaildateForm();
        RxReminderSetting.handleWysihtml5();
    },
    handleWysihtml5: function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["/bundles/admin/assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    },
    vaildateForm: function() {
        $('#cycleOneForm').validate({
            rules: {
                'form[reminder1][durationTime]': {
                    required: true,
                    digits: true
                },
                'form[reminder2][durationTime]': {
                    required: true,
                    digits: true
                },
                'form[reminder3][durationTime]': {
                    required: true,
                    digits: true
                },
                'form[reminder3][expiredTime]': {
                    required: true,
                    digits: true
                }
            },
            submitHandler: function(form) {
                form.submit();
            },
            errorPlacement: function(error, element) {
                return false;
            }
        });
    }
}

$(document).ready(function () {
    RxReminderSetting.init();
});