<?php

namespace Mxb\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Mxb\BackendBundle\Form\RegistrationType;
use Mxb\CoreBundle\Entity\User;
class DefaultController extends Controller
{
    /**
     * @Route("/admin", name="admin_login")
     * @Template("@MxbBackend/Default/login.html.twig")
     */
    public function loginAction()
    {
        return array();
    }
    /**
     * @Route("/admin/register", name="admin_register")
     * @Template("@MxbBackend/Default/register.html.twig")
     */
    public function registerAction(Request $req)
    {
        $form = $this->createForm(RegistrationType::class,new User());
        $em = $this->getDoctrine()->getManager();
        if ($req->getMethod() == 'POST') {
            $form->handleRequest($req);
            $user = $form->getData();
            $pwd = $user->getPassword();
            $encoder = $this->container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($user, $pwd);
            $user->setPassword($pwd);
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('admin_login');

        }
        return array('form' => $form->createView() );
    }
    /**
     * @Route("/login-check", name="login_check")
     * @Template("@MxbBackend/Default/login.html.twig")
     */
    public function loginCheckAction()
    {
        return array();
    }
    /**
     * @Route("/reset-password", name="forgot_password")
     * @Template("@MxbBackend/Default/login.html.twig")
     */
    public function resetkAction()
    {
        return array();
    }
}
