<?php
namespace Mxb\BackendBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('username', TextType::class, ['label'=>'User Name'])
                ->add('password', PasswordType::class,['label'=>'Password'])
                ->add('confirm', PasswordType::class, ['mapped' => false,'label'=>'Re-type password'])
                ->add('save', SubmitType::class, ['label'=>'Register'])
        ;
    }

    public function getName()
    {
        return 'registration';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MxbCoreBundle\Entity\User',
        ]);
    }

}
