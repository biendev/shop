<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UserInfo;
use AppBundle\Entity\Users;
use AppBundle\Entity\Pages as Page;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $fb = new \Facebook\Facebook([
            'app_id' => '1135872536428504', 'app_secret' => '78058ba7e37a56e7d2e5416db666bfd0']);
        $helper = $fb->getRedirectLoginHelper(); // to set redirection url
        $permissions = [
            'public_profile',
            'email',
            'publish_actions',            
            'manage_pages',
            'publish_pages',
            'pages_manage_instant_articles',
            'pages_messaging',
            'pages_messaging_subscriptions',
            'user_posts',
            'user_status'
            ]; // set required permissions to user details
        $url = $request->getUriForPath('/checkLogin');    
        $loginUrl = $helper->getLoginUrl($url, $permissions);
        // replace this example code with whatever you need
        return $this->render('AppBundle:default:index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                    'url' => $loginUrl
        ]);
    }

    /**
     * @Route("/checkLogin", name="checkLogin")
     */
    public function checkLoginAction(Request $request) {

        $fb = new \Facebook\Facebook([
            'app_id' => '1135872536428504', 
            'app_secret' => '78058ba7e37a56e7d2e5416db666bfd0']);
        $helper = $fb->getRedirectLoginHelper(); // to perform operation after redirection
        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }
        try {
            $accessToken = $helper->getAccessToken(); // to fetch access token
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {

            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        if (!isset($accessToken)) {// checks whether access token is in there or not
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo 'Error: ' . $helper->getError() . '\n';
                echo 'Error Code: ' . $helper->getErrorCode() . '\n';
                echo 'Error Reason: ' . $helper->getErrorReason() . '\n';
                echo 'Error Description: ' . $helper->getErrorDescription() . '\n';
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }
        
        try {

            $response = $fb->get('/me?fields=id,name,email', $accessToken->getValue());
        } catch (Facebook\Exceptions\FacebookResponseException $e) {// throws an error if invalid fields are specified
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $user = $response->getGraphUser();
        $em  = $this->getDoctrine()->getManager();
        if(true) {
            $objUser = $em->getRepository("AppBundle:Users")->find(5); 
            $objUser->setToken($accessToken->getValue());
            return $this->redirect('pages');
            exit;
            
        } 
        else {
         // to get user details
       
            $objUser = new Users();
            $userInfo = new UserInfo();
            $objUser->setUserId($user['id']);
            $objUser->setToken($accessToken->getValue());
            $userInfo->setName($user['name']);      
            $userInfo->setEmail($user['email']);
            $objUser->setUserInfo($userInfo);
        }
        $em->persist($objUser);
        $em->flush();
        return $this->redirect('pages');
        exit;
        
    }
     /**
     * @Route("/getData", name="getData")
     */
    public function getDataAction(    ) {
        $em  = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:Users")->find(5);       
        $token = $user->getToken();
        $fb = new \Facebook\Facebook([
            'app_id' => '1135872536428504', 
            'app_secret' => '78058ba7e37a56e7d2e5416db666bfd0']);      
        try {

            $response = $fb->get('/me/accounts?type=page', $token);
       
            foreach($response->getDecodedBody()['data'] as $page){
                $objPage = new Page();
                $objPage->setPageId($page['id']);
                $objPage->setName($page['name']);
                $objPage->setCategory($page['category']);
                $objPage->setToken($page['access_token']);
                $objPage->setUser($user);
                $em->persist($objPage);
                $em->flush();
            
            }
           
            
        } catch (Facebook\Exceptions\FacebookResponseException $e) {// throws an error if invalid fields are specified
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        echo 'done';
        exit();
    }
    
     /**
     * @Route("/pages", name="getDataPages")
     */
    public function addPostToPage(){
        $em  = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AppBundle:Users")->find(5);       
        $token = $user->getToken();
        $data['picture'] = "http://www.example.com/image.jpg";
        $data['link'] = "http://www.example.com/";
        $data['message'] = "Your message";
        $data['caption'] = "Caption";
        $data['description'] = "Description";
        $data['access_token'] = $token;
      
         $fb = new \Facebook\Facebook([
            'app_id' => '1135872536428504', 
            'app_secret' => '78058ba7e37a56e7d2e5416db666bfd0']);   
          $response = $fb->get('/me/accounts?type=page', $token);
          
            foreach($response->getDecodedBody()['data'] as $page){
                try {
                var_dump($page);
                $tokenPage = $page['access_token'];
                var_dump($tokenPage);
                $ret = $fb->post('/'.$page['id'].'/feed',$data,$token);
                 $response = $fb->post('/me/feed', $data, $token);
                var_dump($ret);
                var_dump($response);
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
                }
            }
        
        
                exit();
    }

}
